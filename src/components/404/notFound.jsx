import { Link } from "react-router-dom";
import css from "../404/notFound.module.css";

export default function NotFound(){
    return(
        <>
          <div className={`${css.notFoundCardContainer}`}>
              
            <div className={`${css.notFoundCard}`}>
              <h1>404</h1>
                <p>PAGINA NON TROVATA</p>

                <Link to="/">Torna alla home</Link>
            </div>

            

          </div>
        </>
    )
}