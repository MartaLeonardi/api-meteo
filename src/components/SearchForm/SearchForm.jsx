import { useState } from "react";
import "./SearchForm.css";
import CardMeteo from "../CardMeteo/CardMeteo";

export default function SearchForm() {

  const [citta, setCitta] = useState();

  const [response, setResponse] = useState(null);

  const handleChange = (e) =>{
    const nuovaCitta = e.target.value;
    setCitta(nuovaCitta);
  }

  const handleSubmit = async (e) =>{
    e.preventDefault();
          
    console.log(citta)
    if(citta==undefined){
      alert("Non hai inserito il nome della città")
    }else{
      const fetchedData = await fetchData(citta);
      console.log(fetchedData)

      if(fetchedData===404){
        alert("Città non trovata");
      }else{
        setResponse(fetchedData);
      }

    }

  }
  
  const handleClick = (e) => {
    setResponse(null)
  }

  const fetchData = async (citta) => {
    const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${citta}&appid=43ff2a03c2ddb0bca4135a706251f080&lang=it&units=metric`, {
      mode: "cors",
      method: "GET",
    })

    if(response.status == 200){
      return response.json()
    }else if(response.status == 404){
      return response.status
    }
    
  }


  return (
    <>
 
        <div className="container text-center" id="containerForm">

        {!response && 
        <form className="form" onSubmit={handleSubmit}>
          <p className="heading">Inserisci qui il nome della città</p>
          <input className="input" placeholder="Palermo" type="text" name="citta" value={citta} onChange={handleChange}/>
          <button className="btn" type="submit">Effettua previsione</button>
        </form>
        }
        
        {response && 
        <>
        
          <CardMeteo responseHTTP={response} />
          
          <button onClick={handleClick}>
            <span class="shadow"></span>
            <span class="edge"></span>
            <span class="front text"> Effettua un'altra previsione
            </span>
          </button>

        </>
        }
      </div>

      
    </>
  );
}
