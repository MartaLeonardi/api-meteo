import { useOutlet } from "react-router-dom"
import Navbar from "../components/Navbar/Navbar.jsx"
import Footer from "../components/Footer/Footer.jsx"

export default function Layout () {

const outlet = useOutlet();

return(
    <>
    <div style={{background: "linear-gradient(#1265AA, #52CFD9)", height:"100%", width:"100%", minHeight:"100vh", minWidth:"50vh"}}>
        <Navbar/>
            
                {outlet}
            
        <Footer/>
    </div>
     
    </>
)

}