import { useEffect } from "react";
import css from "./style/home.module.css";
import SearchForm from "../components/SearchForm/SearchForm";

export default function Home() {
  useEffect(() => {
    document.title = "API Meteo - Home";
  }, []);

  return (
    <>
      <div className={`${css.myDivContainer}`}>
        <div className={`${css.container}`}>
          <div className={`${css.cloud} ${css.front}`}>
            <span className={`${css.leftFront}`}></span>
            <span className={`${css.rightFront}`}></span>
          </div>
          <span className={`${css.sun} ${css.sunshine}`}></span>
          <span className={`${css.sun}`}></span>
          <div className={`${css.cloud} ${css.back}`}>
            <span className={`${css.leftBack}`}></span>
            <span className={`${css.rightBack}`}></span>
          </div>
        </div>

        <div>
          <h1>Benvenuto in API Meteo</h1>
          <h3>Il sito che rileva il meteo in tempo reale!</h3>
        </div>
      </div>

     
          <SearchForm />
        
      
    </>
  );
}
