import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import Layout from './components/Layout.jsx';
import Home from './pages/Home.jsx';
import NotFound from "./components/404/notFound.jsx";

const router = createBrowserRouter(
  [
    {
      element:<Layout />,
      children: [
        {
          path:"/",
          element: <Home/>
        },
        {
          path: "*",
          element: <NotFound />,
        }
      ]
    }
  ]
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router} />
)
